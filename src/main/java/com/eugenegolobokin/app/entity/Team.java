package com.eugenegolobokin.app.entity;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "team")
@Data

public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    private String name;

    @OneToMany(mappedBy = "team")
    private Set<Person> persons;

    @OneToMany(mappedBy = "team")
    private Set<Module> modules;

    public Team(String name) {
        this.name = name;
    }

    public Team addModule(Module module) {
        if (modules == null) {
            modules = new HashSet<>();
        }
        modules.add(module);
        return this;
    }

    public Team addPerson(Person person) {
        if (persons == null) {
            persons = new HashSet<>();
        }
        persons.add(person);
        return this;
    }
}
