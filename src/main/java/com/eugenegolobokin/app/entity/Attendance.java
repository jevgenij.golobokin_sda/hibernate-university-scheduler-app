package com.eugenegolobokin.app.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "attendance")
@Data

public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private boolean isPresent;

    @ManyToOne
    @JoinColumn(name = "moduleId")
    private Module module;
    @ManyToOne
    @JoinColumn(name = "studentId")
    private Person student;

    public Attendance(LocalDate date, boolean isPresent) {
        this.date = date;
        this.isPresent = isPresent;
    }
}
