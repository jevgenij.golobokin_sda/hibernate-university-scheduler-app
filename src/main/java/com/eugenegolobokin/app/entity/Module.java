package com.eugenegolobokin.app.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "module")
@Data
@Accessors(chain = true)

public class Module {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name = "teamId")
    private Team team;
    @ManyToOne
    @JoinColumn(name = "topicId")
    private Topic topic;
    @ManyToOne
    @JoinColumn(name = "trainerId")
    private Person trainer;
    @ManyToOne
    @JoinColumn(name = "classroomId")
    private Classroom classroom;
    @OneToMany(mappedBy = "module")
    private Set<Attendance> attendances;

    public Module addAttendance(Attendance attendance) {
        if (attendances == null) {
            this.attendances = new HashSet<>();
        }
        this.attendances.add(attendance);
        return this;
    }

}
