package com.eugenegolobokin.app.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "person")
@Data
@Accessors(chain = true)
@ToString

public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @NaturalId //same as @Column(unique=true)
    private String email;
    private LocalDate dateOfBirth;
    private boolean isTrainer;

    @ManyToOne
    @JoinColumn(name = "teamId")
    private Team team;

    @OneToMany(mappedBy = "student")
    private Set<Attendance> attendances;

    @OneToMany(mappedBy = "trainer")
    private Set<Module> modules;

    public Person addModule(Module module) {
        if (modules == null) {
            modules = new HashSet<>();
        }
        modules.add(module);
        return this;
    }

    public Person addAttendance(Attendance attendance) {
        if (attendances == null) {
            attendances = new HashSet<>();
        }
        attendances.add(attendance);
        return this;
    }

}
