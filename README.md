# University scheduler app

### About the app

**University scheduler app** is a small app building university domain model according to provided schema.
App written as part of Java coursework.
Dump of sample database created with this app is available under *src\main\resources\uni.sql*

![](schema.png)

### How to run
1. Clone or download project
2. Create local DB with name "uni" and check settings in HibernateUtil.java (user/pass/url)
3. Run Main.java to create domain

### Tech used
*Java 13, Apache Maven, Hibernate, Lombok, MySQL*

